#include "TextureUtils.h"
// For printf()
#include <stdio.h>
/**
* Create Texture from File
*
* Function to load a texture from a given filename
* and render context.
*
* @param filename Filename of the file to load.
* @param renderer A pointer to the current SDL_Renderer.
* @return A pointer to the SDL_Texture created, or a
* nullptr on error.
*/
SDL_Texture* createTextureFromFile(const char* filename,
	SDL_Renderer* renderer)
{
	SDL_Texture* texture = nullptr;
	SDL_Surface* temp = nullptr;
	temp = IMG_Load(filename);
	if (temp == nullptr)
	{
		printf("%s image not found!", filename);
	}
	else
	{
		texture = SDL_CreateTextureFromSurface(renderer, temp);
		SDL_FreeSurface(temp);
		temp = nullptr;
	}
	return texture;
}