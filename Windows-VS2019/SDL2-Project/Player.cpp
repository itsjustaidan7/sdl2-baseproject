#include "Player.h"
#include "TextureUtils.h"
/**
* initPlayer
*
* Comment cut for brevity
*/
		
void initPlayer(Player* player, SDL_Renderer* renderer)
{
	// Create player texture from file, optimised for renderer
	player->texture = createTextureFromFile("assets/images/undeadking.png",
		renderer);
	
// Allocate memory for the animation structures
		for (int i = 0; i < player->MAX_ANIMATIONS; i++)
		{
			player->animations[i] = new Animation();
		}
	// Setup the animation structure
		player->animations[LEFT]->init(3, player->SPRITE_WIDTH, player->SPRITE_HEIGHT, -1, 1);
		player->animations[RIGHT]->init(3, player->SPRITE_WIDTH, player->SPRITE_HEIGHT, -1, 2);
		player->animations[UP]->init(3, player->SPRITE_WIDTH, player->SPRITE_HEIGHT, -1, 3);
		player->animations[DOWN]->init(3, player->SPRITE_WIDTH, player->SPRITE_HEIGHT, -1, 0);
		player->animations[IDLE]->init(1, player->SPRITE_WIDTH, player->SPRITE_HEIGHT, -1, 0);
						
		// set the default state to idle
		player->state = IDLE;
	// Target is the same size of the source
	player->targetRectangle.w = player->SPRITE_WIDTH;
	player->targetRectangle.h = player->SPRITE_HEIGHT;
}

		



/**
* processInput
*
* Function to process inputs for the player structure
* Note: Need to think about other forms of input!
*
* @param player Player structure processing the input
* @param keyStates The keystates array.
*/
void processInput(Player* player, const Uint8* keyStates)
{
	// Process Player Input
	//Input - keys/joysticks?
	float verticalInput = 0.0f;
	float horizontalInput = 0.0f;
	// If no keys are down player should not animate!
	player->state = IDLE;
	// This could be more complex, e.g. increasing the vertical
	// input while the key is held down.
	if (keyStates[SDL_SCANCODE_UP])
	{
		verticalInput = -1.0f;
		player->state = UP;
	}
	if (keyStates[SDL_SCANCODE_DOWN])
	{
		verticalInput = 1.0f;
		player->state = DOWN;
	}
	if (keyStates[SDL_SCANCODE_RIGHT])
	{
		horizontalInput = 1.0f;
		player->state = RIGHT;
	}
	if (keyStates[SDL_SCANCODE_LEFT])
	{
		horizontalInput = -1.0f;
		player->state = LEFT;
	}
	// Calculate player velocity.
	// Note: This is imperfect, no account taken of diagonal!
	player->vy = verticalInput * player->speed;
	player->vx = horizontalInput * player->speed;
}

void updatePlayer(Player* player, float timeDeltaInSeconds)
{
	// Calculate distance travelled since last update
	float yMovement = timeDeltaInSeconds * player->vy;
	float xMovement = timeDeltaInSeconds * player->vx;
	// Update player position.
	player->x += xMovement;
	player->y += yMovement;
	// Move sprite to nearest pixel location.
	player->targetRectangle.y = round(player->y);
	player->targetRectangle.x = round(player->x);
	// Get current animation
	Animation* current = player->animations[player->state];
	// let animation update itself.
	current->update(timeDeltaInSeconds);
}


/**
* drawPlayer
*
* Function draw a Player structure
*
* @param player Player structure tp draw
* @param renderer SDL_Renderer to draw to
*/
void drawPlayer(Player* player, SDL_Renderer* renderer)
{
	// Get current animation based on the state.
	Animation* current = player->animations[player->state];
	SDL_RenderCopy(renderer, player->texture, current->getCurrentFrame(), &player->targetRectangle);
}


/*
Rest of function and other player function ... omitted for brevity
*//**
* destPlayer
*
* Function to clean up an player structure.
*
* @param player Player structure to destroy
*/
void destPlayer(Player* player)
{
	// Clean up animations - free memory
	for (int i = 0; i < player->MAX_ANIMATIONS; i++)
	{
		// Clean up the animaton object
		// allocated with new so use delete.
		delete player->animations[i];
		player->animations[i] = nullptr;
	}
	// Clean up
	SDL_DestroyTexture(player->texture);
	player->texture = nullptr;
}